Feature: Validate read bin functionality on bin api endpoint

  @regression @readBinUsingBinId
  Scenario: Validate user is able to read bin record successfully using bin id
    When User call the bin endpoint to read bin using bin id "604264970866664b108962d5" with response "200"
    Then the schema should match with the specification defined in "read_bin.json"
    Then response should have header "Content-Type" with value "application/json; charset=utf-8"
    And  response should have valid cookie
    And response should have record "Test Bin Creation43294"

  @regression @readBindWithInvalidBinId
  Scenario: Validate user receives error message when trying to read bin with invalid bin id
    When User call the bin endpoint to read bin using bin id "604264970866664b108962e5" with response "404"
    Then the schema should match with the specification defined in "bin_error.json"
    Then response should have header "Content-Type" with value "application/json; charset=utf-8"
    Then error message should be "Bin not found or it doesn't belong to your account"