Feature: Validate update bin functionality on bin api endpoint

  @regression @updateBinUsingBinId
  Scenario: Validate user is able to update bin record successfully using bin id
    When User call the bin endpoint to update bin using bin id "60426dff81087a6a8b96f08a" with response "200"
    Then the schema should match with the specification defined in "update_bin.json"
    Then response should have header "Content-Type" with value "application/json; charset=utf-8"
    And  response should have valid cookie
    And response should have message with updated bin record

  @regression @updateBinUsingInvalidBinId
  Scenario: Validate user is able to update bin record successfully using bin id
    When User call the bin endpoint to update bin using bin id "60426dff81087a6a8b96f08b" with response "404"
    Then the schema should match with the specification defined in "bin_error.json"
    Then response should have header "Content-Type" with value "application/json; charset=utf-8"
    Then error message should be "Bin not found"