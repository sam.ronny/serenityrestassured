Feature: Validate create bin functionality on bin api endpoint

  @regression @CreateBin
  Scenario: Validate successful creation of bin using bin endpoint
    When User call the bin endpoint to create bin with response "200"
    Then the schema should match with the specification defined in "created_bin.json"
    And response should have header "Content-Type" with value "application/json; charset=utf-8"
    And response should have message with created bin record

  @CreateBinWithEmptyBinRecord @regression
  Scenario: Validate error message is displayed when bin is sent blank
    When User call the bin endpoint with empty record to create bin with response "400"
    Then the schema should match with the specification defined in "bin_error.json"
    Then error message should be "Bin cannot be blank"
