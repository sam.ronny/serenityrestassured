package com.bin.serenity.stepdefinitions;

import com.github.javafaker.Faker;
import com.bin.serenity.bin.BinActions;
import com.bin.serenity.bin.BinResponseValidation;
import com.bin.serenity.commonUtilities.CommonValidations;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import net.serenitybdd.core.Serenity;
import net.thucydides.core.annotations.Steps;

import java.util.HashMap;
import java.util.Map;

import static net.serenitybdd.rest.SerenityRest.lastResponse;

public class binSteps {
    @Steps
    BinActions binActions;

    @Steps
    BinResponseValidation binResponseValidation;
    @Steps
    CommonValidations commonValidations;

    Faker createBin = new Faker();

    @Then("User call the bin endpoint to create bin with response \"(.*)\"")
    public void createBinWithValidRecord(String status) {
        Map<String, Object> jsonAsMap = new HashMap<>();
        int randomBinValue = createBin.random().nextInt(33333, 44444);
        String binRecord = "Test Bin Creation" + randomBinValue;
        Serenity.setSessionVariable("createdBinRecord").to(binRecord);
        jsonAsMap.put("validate", binRecord);
        binActions.createBin(jsonAsMap);
        commonValidations.responseCodeIs(Integer.parseInt(status), lastResponse());
    }

    @Then("User call the bin endpoint to read bin using bin id \"(.*)\" with response \"(.*)\"")
    public void readBinWithValidBinId(String id, String status) {
        binActions.readBin(id);
        commonValidations.responseCodeIs(Integer.parseInt(status), lastResponse());
    }

    @Then("User call the bin endpoint to update bin using bin id \"(.*)\" with response \"(.*)\"")
    public void updateBinWithValidBinId(String id, String status) {
        Map<String, Object> jsonAsMap = new HashMap<>();

        int randomBinValue = createBin.random().nextInt(33333, 44444);
        String binRecord = "Bin update" + randomBinValue;
        Serenity.setSessionVariable("updatedBinRecord").to(binRecord);
        jsonAsMap.put("validate", binRecord);
        binActions.updateBin(id,jsonAsMap);
        commonValidations.responseCodeIs(Integer.parseInt(status), lastResponse());
    }


    @Then("User call the bin endpoint with empty record to create bin with response \"(.*)\"")
    public void createBinWithInvalidRecord(String status) {
        Map<String, Object> jsonAsMap = new HashMap<>();
        binActions.createBin(jsonAsMap);
        commonValidations.responseCodeIs(Integer.parseInt(status), lastResponse());
    }


    @And("response should have message with created bin record")
    public void validateCreateUserResponse() {
        binResponseValidation.validateCreatedUserResponseMessage(lastResponse());
    }

    @And("response should have message with updated bin record")
    public void validateUpdatedUserResponse() {
        binResponseValidation.validateUpdatedUserResponseMessage(lastResponse());
    }

    @And("response should have record \"(.*)\"")
    public void validateRecord(String content) {
        commonValidations.verifyResponseBodyContent(content, lastResponse());
    }


}
