package com.bin.serenity.bin;

import io.restassured.response.Response;
import net.serenitybdd.core.Serenity;
import net.thucydides.core.annotations.Step;

import static org.assertj.core.api.Assertions.assertThat;

public class BinResponseValidation {

    @Step("Validate bin with created record is present in response")
    public void validateCreatedUserResponseMessage(Response lastResponse) {
        assertThat(getRecordFromResponse(lastResponse)).isEqualToIgnoringCase(Serenity.sessionVariableCalled("createdBinRecord").toString());
    }


    @Step("Get bin record from response")
    public String getRecordFromResponse(Response binCreatedResponse) {
        return (binCreatedResponse.getBody().jsonPath().getString("record.validate"));
    }

    @Step("Validate bin with updated record is present in response")
    public void validateUpdatedUserResponseMessage(Response lastResponse) {
        assertThat(getRecordFromResponse(lastResponse)).isEqualToIgnoringCase(Serenity.sessionVariableCalled("updatedBinRecord").toString());
    }
}
