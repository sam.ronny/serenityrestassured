package com.bin.serenity.bin;

import com.bin.serenity.commonUtilities.CommonRequestSpec;
import io.restassured.response.Response;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Step;

import java.util.Map;

public class BinActions {
    @Step("Request to create bin with record {0}")
    public Response createBin(Map<String, Object> jsonAsMap) {
        return SerenityRest.given().spec(CommonRequestSpec.requestSpecification()).
                body(jsonAsMap).basePath("b").log().all().when().post();
    }

    @Step("Request to read bin with bin id {0}")
    public Response readBin(String id) {
        return SerenityRest.given().spec(CommonRequestSpec.requestSpecification()).
                basePath("b").get(id).then().log().all().extract().response();
    }

    @Step("Request to update bin with bin id {0}")
    public Response updateBin(String id,Map<String, Object> jsonAsMap) {
        String path = "b/" + id;
        return SerenityRest.given().spec(CommonRequestSpec.requestSpecification()).
                body(jsonAsMap).basePath(path).log().all().when().put();
    }
}
