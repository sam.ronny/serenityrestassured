package com.bin.serenity.commonUtilities;

import io.restassured.builder.RequestSpecBuilder;
import io.restassured.specification.RequestSpecification;
import net.serenitybdd.core.environment.EnvironmentSpecificConfiguration;
import net.thucydides.core.guice.Injectors;
import net.thucydides.core.util.EnvironmentVariables;

public class CommonRequestSpec {
  /**
   * Get Request Specification for JSONBIN.io endpoint
   *
   * @return RequestSpecification
   */

  public static Configuration configuration = ConfigurationManager.getConfiguration();
  public static RequestSpecification requestSpecification() {
    EnvironmentVariables environmentVariables = Injectors.getInjector()
        .getInstance(EnvironmentVariables.class);

    String baseUrl = EnvironmentSpecificConfiguration.from(environmentVariables)
        .getProperty("baseurl");

    return new RequestSpecBuilder().setBaseUri(baseUrl)
        .setContentType("application/json").addHeader("X-Master-Key",configuration.secret_key())
        .build();
  }
}
