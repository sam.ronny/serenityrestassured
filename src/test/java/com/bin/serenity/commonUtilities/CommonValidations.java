package com.bin.serenity.commonUtilities;

import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchemaInClasspath;
import static net.serenitybdd.rest.SerenityRest.lastResponse;
import static org.assertj.core.api.Assertions.assertThat;

import cucumber.api.java.en.And;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;
import net.thucydides.core.annotations.Step;
import org.junit.Assert;

public class CommonValidations {

    @Step("Verify that API response is {0}")
    public void responseCodeIs(int responseCode, Response lastResponse) {
        assertThat(lastResponse.statusCode()).isEqualTo(responseCode);
    }

    @Step("Verify that response is empty list")
    public void responseShouldBeEmptyList(Response lastResponse) {
        assertThat(lastResponse.getBody().jsonPath().getList("").size()).isEqualTo(0);
    }

    @Step("Verify response schema with {1}")
    public void verifyResponseSchema(Response lastResponse, String schemaJson) {
        lastResponse.then().body(matchesJsonSchemaInClasspath("schema/" + schemaJson));
    }

    @Step("Verify error message from response is {0}")
    public void verifyMessageResponse(String message, Response lastResponse) {
        assertThat(lastResponse.getBody().jsonPath().getString("message")).isEqualToIgnoringCase(message);
    }

    @Step("Verify response header value of {0}")
    public void verifyHeaderValueOfHeaderType(String headerType, String headerValue, Response lastResponse) {
        String contentValue = lastResponse.header(headerType);
        Assert.assertEquals(contentValue, headerValue);
    }

    @Step("Verify response has cookie and it is not empty")
    public void verifyCookie(Response lastResponse) {
        String cookieValue = lastResponse.getCookie("token");
        assertThat(cookieValue).isNotBlank();
    }

    @Step("response should have record {0}")
    public void verifyResponseBodyContent(String content, Response lastResponse) {
        Assert.assertEquals(getResponseBody(lastResponse).contains(content), true);
    }

    @Step("Get response body as string")
    public String getResponseBody(Response response) {
        ResponseBody body = response.getBody();
        return body.asString();
    }


}
