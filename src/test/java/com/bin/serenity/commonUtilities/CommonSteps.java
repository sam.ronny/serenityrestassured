package com.bin.serenity.commonUtilities;

import cucumber.api.java.en.And;
import net.thucydides.core.annotations.Steps;

import static net.serenitybdd.rest.SerenityRest.lastResponse;

public class CommonSteps {

    @Steps
    CommonValidations commonValidations;

    @And("response should have header \"(.*)\" with value \"(.*)\"")
    public void validateResponseHeader(String contentHeader,String contentHeaderValue){
        commonValidations.verifyHeaderValueOfHeaderType(contentHeader,contentHeaderValue,lastResponse());
    }

    @And("response should have valid cookie")
    public void validateCookieInResponse() {
        commonValidations.verifyCookie(lastResponse());
    }

    @And("error message should be \"(.*)\"")
    public void messageShouldBe(String errorMessage) {
        commonValidations.verifyMessageResponse(errorMessage, lastResponse());
    }

    @And("the schema should match with the specification defined in \"(.*)\"")
    public void the_schema_should_match_with_the_specification(String spec) {
        commonValidations.verifyResponseSchema(lastResponse(), spec);
    }


}
