# Java-Serenity-RestAssured-Cucumber-JUnit-Maven API Framework

## Introduction

Rest API test framework for bin endpoints available in https://api.jsonbin.io/v3. 

Tests are written using a combination of SerenityBDD, RestAssured, Cucumber, Junit & Maven.

## Framework & Design Considerations
- Serenity BDD is a library that makes it easier to write high quality automated acceptance tests, with powerful reporting and living documentation features. It has strong support for both web testing with Selenium, and API testing using RestAssured.
- API calls & validations are made using RestAssured and SerenityRest which is a wrapper on top of RestAssured. 
- Tests are written in BDD Gherkin format in Cucumber feature files and it is represented as a living documentation in the test report.
- Each domain package consist of an Action class where API actions are defined and another ResponseValidation class where user assertions are written.
- These domain models are called from a step-definitions class which are in-turn called from BDD tests.
- A test scenario to validate API response schema has been included for each endpoint in the respective feature file. The API spec for schema comparison is placed inside "schema" folder in test resources. The specs are generated from https://www.liquid-technologies.com/online-json-to-schema-converter.

### The project directory structure

```Gherkin
src
  + test
    + java                                      Test runners and supporting code
      + bin                                     Domain model package consisting of all actions/validations on bin CRUD functionality
          BinActions                            API calls/User actions on bin APIs
          BinResponseValidations                User validations on bin API response
      + commonutilies                           Package for all common actions and validations
          CommonValidations                     All common validations across all the domain models
          CommonRequestSpec                     Common Request Spec for the API calls
      + stepdefinitions                         Step definitions for the BDD feature
      + commonutilies                           Common utility methods
    + resources
      + features                                Feature files directory
          validate_create_bin.feature                    Feature containing BDD scenarios
      + schema                                  Folder containing json schema for API schema validation
      Serenity.conf                             Configurations file


```
## Getting Started. Required software.
- Install JDK 8 or above
- Install IntelliJ (the latest Community edition)
- Install Maven
- Install Git

## Cloning & Importing the Project
- Open intellij and choose ```File - > Project from version control``` option in intellij
- Choose Git as option from a dropdown and specify the git url as ```https://gitlab.com/sam.ronny/serenityrestassured.git```
- import the project in IntelliJ and wait for all the dependencies to be downloaded


## Executing the tests
- Run `mvn clean verify` from the command line.
- The test results will be recorded in `target/site/serenity/index.html`.
- Please run the below command from root directory to open the result after execution for mac and linux os.
```bash
open target/site/serenity/index.html 
```
- For windows, use ```start target/site/serenity/index.html``` command
- The report records the API calls and its response in a very readable format as shown below.
  ![Screenshot_2021-03-07_at_19.09.31](/uploads/18186ab8c34f59545c93736d477a5dcf/Screenshot_2021-03-07_at_19.09.31.png)

- Each step in tests are very clearly documented for readability and debugging in case of failures.

  ![Screenshot_2021-03-07_at_19.10.24](/uploads/fd55348cb4445158508297c8cdd1ff84/Screenshot_2021-03-07_at_19.10.24.png)

## Artifacts from GitLab CI

Reports can be seen in for each run with gitlabci pipeline, download the artifacts and the html report is present
under `target/site/serenity/index.html`.

Here is the direct link to one of the builds:


https://gitlab.com/sam.ronny/serenityrestassured/-/jobs/1079084641